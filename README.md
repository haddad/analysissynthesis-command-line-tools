A set of low-level command line tools from [Analysis/Synthesis Research Team](https://www.ircam.fr/recherche/equipes-recherche/anasyn/).

This package is already included in the [AudioSculpt](https://forum.ircam.fr/projects/detail/audiosculpt/) software.

Only available in Premium Subscription.
